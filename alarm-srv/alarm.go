package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	pb "gitlab.com/zenyukgo/mst/alarm"
)

var totalPushes int
var averagePush float32
var nextPush []struct{}

func (s *alarmServer) Push(ctx context.Context, in *pb.PushReq) (*pb.Empty, error) {
	log.Info("Push was called")
	averagePush = (averagePush * float32(totalPushes) + in.Num) / float32(totalPushes + 1)
	totalPushes++
	nextPush = append(nextPush, struct{}{})
	return nil, nil
}

func (s *alarmServer) GetAvgAlarmStream(in *pb.GetAvgAlarmStreamReq, stream pb.AvgAlarmService_GetAvgAlarmStreamServer) error {
	log.Info("GetAvgAlarmStream was called")
	for _, _ = range nextPush {
		if averagePush > in.Threshold {
			err := stream.Send(&pb.GetAvgAlarmStreamRes{
				Avg: averagePush,
			})
			if err != nil {
				log.Error("Can't send message back to client")
				return nil
			}
		}
	}
	return nil
}

